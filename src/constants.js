const entitys = {
  users: 'USERS',
  boards: 'BOARDS',
  tasks: 'TASKS'
};

const dbTypes = {
  mongodb: 'MONGODB'
};

module.exports = { entitys, dbTypes };
