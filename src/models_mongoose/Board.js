const mongoose = require('mongoose');

const { getMongooseNativeIdField } = require('../utils');

const { Schema } = mongoose;

const Board = new Schema({
  title: String,
  columns: Array
});

Board.statics.toResponse = (board = {}) => {
  const { id, title, columns } = board;
  return { id, title, columns };
};

Board.virtual('id').get(getMongooseNativeIdField);

Board.set('toJSON', {
  virtuals: true
});

module.exports = mongoose.model('Board', Board);
