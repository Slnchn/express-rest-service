const mongoose = require('mongoose');

const { getMongooseNativeIdField } = require('../utils');

const { Schema } = mongoose;

const Task = new Schema({
  title: String,
  order: Number,
  description: String,
  userId: String,
  boardId: { type: String, default: undefined },
  columnId: String
});

Task.statics.toResponse = (task = {}) => {
  const { id, title, order, description, userId, boardId, columnId } = task;
  return { id, title, order, description, userId, boardId, columnId };
};

Task.virtual('id').get(getMongooseNativeIdField);

Task.set('toJSON', {
  virtuals: true
});

module.exports = mongoose.model('Task', Task);
