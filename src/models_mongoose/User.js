const mongoose = require('mongoose');

const { getMongooseNativeIdField } = require('../utils');

const { Schema } = mongoose;

const User = new Schema({
  name: String,
  login: String,
  password: String
});

User.statics.toResponse = (user = {}) => {
  const { id, name, login } = user;
  return { id, name, login };
};

User.virtual('id').get(getMongooseNativeIdField);

User.set('toJSON', {
  virtuals: true
});

module.exports = mongoose.model('User', User);
