const path = require('path');

const express = require('express');
const swaggerUI = require('swagger-ui-express');
const YAML = require('yamljs');

const loginRouter = require('./routers/login.router');
const userRouter = require('./routers/user.router');
const boardRouter = require('./routers/board.router');
const taskRouter = require('./routers/task.router');

const { Logger, logOutput } = require('./Logger');

const { authMiddleware, errorHandlingMiddleware, loggingMiddleware } = require('./middlewares');

const app = express();
const swaggerDocument = YAML.load(path.join(__dirname, '../doc/api.yaml'));

const logger = new Logger({ output: logOutput.FILE });
logger.init();

app.use(express.json());
app.use(loggingMiddleware.bind(null, logger));
app.use('/doc', swaggerUI.serve, swaggerUI.setup(swaggerDocument));

app.use('/', (req, res, next) => {
  if (req.originalUrl === '/') {
    res.send('Service is running!');
    return;
  }
  next();
});

app.use('/login', loginRouter);
app.use('/users', authMiddleware, userRouter);
app.use('/boards', authMiddleware, boardRouter);
boardRouter.use('/:boardId/tasks', authMiddleware, taskRouter);

app.use(errorHandlingMiddleware.bind(null, logger));

process.on('unhandledRejection', error => {
  logger.error(
    `Critical unhandled promise rejection. Saving state and closing app. ${error.message} ${error.stack}`
  );

  // throw an error to crash the app
  throw error;
});

process.on('uncaughtException', error => {
  logger.error(
    `Critical uncaught error. Saving state and closing app. ${error.message} ${error.stack}`
  );

  // throw an error to crash the app
  throw error;
});

module.exports = app;
