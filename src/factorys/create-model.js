const MongooseBoard = require('../models_mongoose/Board');
const MongooseUser = require('../models_mongoose/User');
const MongooseTask = require('../models_mongoose/Task');

const { entitys } = require('../constants');

function getMongooseModel(entity) {
  switch (entity) {
    case entitys.boards: {
      return MongooseBoard;
    }
    case entitys.users: {
      return MongooseUser;
    }
    case entitys.tasks: {
      return MongooseTask;
    }
    default: {
      return Object;
    }
  }
}

module.exports = (dbType, entity) => {
  // should be switch construction with different model-fatorys: getMongooseModel, getInMemoryModel etc
  return getMongooseModel(entity);
};
