const DbConnector = require('../db-connectors/DbConnector');
const MongoDbConnector = require('../db-connectors/mongodb/MongoDbConnector');

const createModel = require('../factorys/create-model');

const { dbTypes } = require('../constants');

const { DB_TYPE } = require('../common/config');

module.exports = (dbType, entity) => {
  switch (dbType) {
    case dbTypes.mongodb: {
      return new MongoDbConnector(createModel(DB_TYPE, entity));
    }
    default: {
      return new DbConnector(entity);
    }
  }
};
