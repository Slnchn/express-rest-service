function getMongooseNativeIdField() {
  return this._id;
}

module.exports = { getMongooseNativeIdField };
