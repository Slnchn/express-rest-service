const taskService = require('./task.service');

const createDbConnector = require('../factorys/create-db-connector');
const createModel = require('../factorys/create-model');

const { CustomRestError } = require('../errors/CustomRestError');

const { entitys } = require('../constants');

const { DB_TYPE } = require('../common/config');

const boardDbConnector = createDbConnector(DB_TYPE, entitys.boards);
const Board = createModel(DB_TYPE, entitys.boards);

module.exports = {
  getBoards() {
    return boardDbConnector.find();
  },

  async getBoard(boardId) {
    const resultBoard = (await boardDbConnector.find({ _id: boardId }))[0];
    if (resultBoard) {
      return resultBoard;
    }

    throw new CustomRestError({ message: 'Board not found.' });
  },

  async createBoard(newBoardParams) {
    const newBoard = new Board(newBoardParams);

    await boardDbConnector.insert(newBoard);

    return this.getBoard(newBoard._id);
  },

  async updateBoard(boardId, boardUpdates) {
    const updatedBoard = boardDbConnector.update({ _id: boardId }, boardUpdates);
    if (updatedBoard) {
      return updatedBoard;
    }

    throw new CustomRestError({ message: 'Board not found.' });
  },

  async deleteBoard(boardId) {
    const deletedBoard = boardDbConnector.delete({ _id: boardId });
    if (deletedBoard) {
      await taskService.deleteWhere({ boardId });

      return deletedBoard;
    }

    throw new CustomRestError({ message: 'Board not found.' });
  }
};
