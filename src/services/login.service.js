const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { UNAUTHORIZED } = require('http-status-codes');

const { CustomRestError } = require('../errors/CustomRestError');

const userService = require('./user.service');

module.exports = {
  async auth(login, password) {
    const user = await userService.getUserByLogin(login);
    if (user) {
      const isPasswordCorrect = await bcrypt.compare(password, user.password);
      if (isPasswordCorrect) {
        return jwt.sign({ login, id: user.id }, process.env.JWT_SECRET_KEY);
      }

      throw new CustomRestError({ status: UNAUTHORIZED, message: 'Wrong password' });
    }

    throw new CustomRestError({ status: UNAUTHORIZED, message: 'No user with this login' });
  }
};
