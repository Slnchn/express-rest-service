const bcrypt = require('bcrypt');
const { CONFLICT } = require('http-status-codes');

const createDbConnector = require('../factorys/create-db-connector');
const createModel = require('../factorys/create-model');

const taskService = require('./task.service');

const { CustomRestError } = require('../errors/CustomRestError');

const { entitys } = require('../constants');

const { DB_TYPE } = require('../common/config');

const userDbConnector = createDbConnector(DB_TYPE, entitys.users);
const User = createModel(DB_TYPE, entitys.users);

module.exports = {
  getUsers() {
    return userDbConnector.find();
  },

  async getUser(userId) {
    return (await userDbConnector.find({ _id: userId }))[0];
  },

  async getUserByLogin(login) {
    return (await userDbConnector.find({ login }))[0];
  },

  async createUser(newUserParams) {
    if (!(await this.getUserByLogin(newUserParams.login))) {
      const cryptedPassword = await bcrypt.hash(newUserParams.password, 2);
      const newUser = new User({ ...newUserParams, password: cryptedPassword });

      await userDbConnector.insert(newUser);

      return this.getUser(newUser._id);
    }

    throw new CustomRestError({ status: CONFLICT, message: 'User with this login already exists' });
  },

  async updateUser(userId, userUpdates) {
    const updatedUser = await userDbConnector.update({ _id: userId }, userUpdates);
    if (updatedUser) {
      return updatedUser;
    }

    throw new CustomRestError({ message: 'Task not found.' });
  },

  async deleteUser(userId) {
    const deletedUser = await userDbConnector.delete({ _id: userId });
    if (deletedUser) {
      await taskService.updateTasks({ userId }, { userId: null });

      return deletedUser;
    }

    throw new CustomRestError({ message: 'Task not found.' });
  }
};
