const createDbConnector = require('../factorys/create-db-connector');
const createModel = require('../factorys/create-model');

const { CustomRestError } = require('../errors/CustomRestError');

const { entitys } = require('../constants');

const { DB_TYPE } = require('../common/config');

const taskDbConnector = createDbConnector(DB_TYPE, entitys.tasks);
const Task = createModel(DB_TYPE, entitys.tasks);

module.exports = {
  getTasks() {
    return taskDbConnector.find({});
  },

  async getBoardTasks(boardId) {
    return await taskDbConnector.find({ boardId });
  },

  async getTask(boardId, taskId) {
    const task = (await taskDbConnector.find({ _id: taskId, boardId }))[0];
    if (task) {
      return task;
    }

    throw new CustomRestError({ message: 'Task not found.' });
  },

  async createTask(boardId, newTaskParams) {
    const newTask = new Task({ ...newTaskParams, boardId });

    await taskDbConnector.insert(newTask);

    return this.getTask(boardId, newTask._id);
  },

  async updateTask(boardId, taskId, taskUpdates) {
    const updatedTask = await taskDbConnector.update({ _id: taskId, boardId }, taskUpdates);
    if (updatedTask) {
      return updatedTask;
    }

    throw new CustomRestError({ message: 'Task not found.' });
  },

  async updateTasks(updateCondition, taskUpdates) {
    return taskDbConnector.updateMany(updateCondition, taskUpdates);
  },

  async deleteTask(taskId) {
    const deletedTask = taskDbConnector.delete({ _id: taskId });
    if (deletedTask) {
      return deletedTask;
    }

    throw new CustomRestError({ message: 'Task not found.' });
  },

  async deleteWhere(deleteCondition) {
    return taskDbConnector.deleteMany(deleteCondition);
  }
};
