const jwt = require('jsonwebtoken');
const { INTERNAL_SERVER_ERROR, UNAUTHORIZED, getReasonPhrase } = require('http-status-codes');

const { CustomRestError } = require('./errors/CustomRestError');

function authMiddleware(req, res, next) {
  const tokenString = req.header('Authorization');
  if (tokenString) {
    const [type, token] = tokenString.split(' ');
    if (type !== 'Bearer') {
      throw new CustomRestError({ status: UNAUTHORIZED, message: 'Wrong auth schema' });
    }

    jwt.verify(token, process.env.JWT_SECRET_KEY, err => {
      if (err) {
        throw new CustomRestError({ status: UNAUTHORIZED, message: 'Invalid token' });
      }
    });

    return next();
  }

  throw new CustomRestError({ status: UNAUTHORIZED, message: 'Auth string not specified' });
}

function loggingMiddleware(logger, req, res, next) {
  logger.info(
    `'${req.method} ${req.url} ${JSON.stringify(req.body)} ${JSON.stringify(req.query)}'`
  );

  next();
}

function errorCatchDecorator(handler) {
  return async (req, res, next) => {
    try {
      await handler(req, res, next);
    } catch (error) {
      return next(error);
    }
  };
}

function errorHandlingMiddleware(logger, error, req, res, next) {
  switch (true) {
    case error instanceof CustomRestError: {
      logger.warn(
        `'${req.method} ${req.url} ${JSON.stringify(req.body)}' -> ${error.status} 
        ${error.message} ${error.stack}`
      );

      res.status(error.status).send(error.message);
      break;
    }
    default: {
      const errorReasonPhrase = getReasonPhrase(INTERNAL_SERVER_ERROR);
      logger.error(
        `'${req.method} ${req.url} ${JSON.stringify(
          req.body
        )}' -> ${INTERNAL_SERVER_ERROR} ${errorReasonPhrase} ${error.message} ${error.stack}`
      );

      res.status(INTERNAL_SERVER_ERROR).send(errorReasonPhrase);
    }
  }

  next();
}

module.exports = {
  authMiddleware,
  errorCatchDecorator,
  errorHandlingMiddleware,
  loggingMiddleware
};
