const mongoose = require('mongoose');

const DbConnector = require('../DbConnector');

mongoose.connect(process.env.MONGO_CONNECTION_STRING, {
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true
});

class MongoDbConnector extends DbConnector {
  constructor(model) {
    super();

    this.model = model;
  }

  findById(id) {
    return this.model.findById(id);
  }

  insert(data) {
    return this.model.create(data);
  }

  find(query) {
    return this.model.find(query);
  }

  update(query, updates) {
    return this.model.findOneAndUpdate(query, updates);
  }

  updateMany(query, updates) {
    return this.model.updateMany(query, updates);
  }

  delete(query) {
    return this.model.deleteOne(query);
  }

  deleteMany(query) {
    return this.model.deleteMany(query);
  }
}

module.exports = MongoDbConnector;
