class DbConnector {
  insert() {}

  find() {}

  update() {}

  updateMany() {}

  delete() {}

  deleteMany() {}

  read() {
    return Promise.reject(new Error('Abstract method "read" of the "DbConnector" used.'));
  }

  write() {
    return Promise.reject(new Error('Abstract method "write" of the "DbConnector" used.'));
  }
}

module.exports = DbConnector;
