const router = require('express').Router({ mergeParams: true });

const createModel = require('../factorys/create-model');

const taskService = require('../services/task.service');

const { errorCatchDecorator } = require('../middlewares');

const { entitys } = require('../constants');

const { DB_TYPE } = require('../common/config');

const Task = createModel(DB_TYPE, entitys.tasks);

router.route('/').get(
  errorCatchDecorator(async (req, res) => {
    const tasks = await taskService.getBoardTasks(req.params.boardId);
    res.json(tasks.map(Task.toResponse));
  })
);

router.route('/:taskId').get(
  errorCatchDecorator(async (req, res) => {
    const task = await taskService.getTask(req.params.boardId, req.params.taskId);
    res.json(Task.toResponse(task));
  })
);

router.route('/').post(
  errorCatchDecorator(async (req, res) => {
    const createdTask = await taskService.createTask(req.params.boardId, req.body);
    res.json(Task.toResponse(createdTask));
  })
);

router.route('/:taskId').put(
  errorCatchDecorator(async (req, res) => {
    const updatedTask = await taskService.updateTask(
      req.params.boardId,
      req.params.taskId,
      req.body
    );

    res.json(Task.toResponse(updatedTask));
  })
);

router.route('/:taskId').delete(
  errorCatchDecorator(async (req, res) => {
    const deletedTask = await taskService.deleteTask(req.params.taskId);
    res.json(Task.toResponse(deletedTask));
  })
);

module.exports = router;
