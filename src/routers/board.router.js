const router = require('express').Router();

const createModel = require('../factorys/create-model');

const boardService = require('../services/board.service');

const { errorCatchDecorator } = require('../middlewares');

const { entitys } = require('../constants');

const { DB_TYPE } = require('../common/config');

const Board = createModel(DB_TYPE, entitys.boards);

router.route('/').get(
  errorCatchDecorator(async (req, res) => {
    const boards = await boardService.getBoards();
    res.json(boards.map(Board.toResponse));
  })
);

router.route('/:boardId').get(
  errorCatchDecorator(async (req, res) => {
    const board = await boardService.getBoard(req.params.boardId);
    res.json(Board.toResponse(board));
  })
);

router.route('/').post(
  errorCatchDecorator(async (req, res) => {
    const createdBoard = await boardService.createBoard(req.body);
    res.json(Board.toResponse(createdBoard));
  })
);

router.route('/:boardId').put(
  errorCatchDecorator(async (req, res) => {
    const updatedBoard = await boardService.updateBoard(req.params.boardId, req.body);
    res.json(Board.toResponse(updatedBoard));
  })
);

router.route('/:boardId').delete(
  errorCatchDecorator(async (req, res) => {
    const deletedBoard = await boardService.deleteBoard(req.params.boardId);
    res.json(Board.toResponse(deletedBoard));
  })
);

module.exports = router;
