const router = require('express').Router();

const createModel = require('../factorys/create-model');

const userService = require('../services/user.service');

const { errorCatchDecorator } = require('../middlewares');

const { entitys } = require('../constants');

const { DB_TYPE } = require('../common/config');

const User = createModel(DB_TYPE, entitys.users);

router.route('/').get(
  errorCatchDecorator(async (req, res) => {
    const users = await userService.getUsers();
    res.json(users.map(User.toResponse));
  })
);

router.route('/:userId').get(
  errorCatchDecorator(async (req, res) => {
    const user = await userService.getUser(req.params.userId);
    res.json(User.toResponse(user));
  })
);

router.route('/').post(
  errorCatchDecorator(async (req, res) => {
    const createdUser = await userService.createUser(req.body);
    res.json(User.toResponse(createdUser));
  })
);

router.route('/:userId').put(
  errorCatchDecorator(async (req, res) => {
    const updatedUser = await userService.updateUser(req.params.userId, req.body);
    res.json(User.toResponse(updatedUser));
  })
);

router.route('/:userId').delete(
  errorCatchDecorator(async (req, res) => {
    const deletedUser = await userService.deleteUser(req.params.userId);
    res.json(User.toResponse(deletedUser));
  })
);

module.exports = router;
