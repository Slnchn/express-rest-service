const router = require('express').Router({ mergeParams: true });

const loginService = require('../services/login.service');

const { errorCatchDecorator } = require('../middlewares');

router.route('/').post(
  errorCatchDecorator(async (req, res) => {
    const { login, password } = req.body;
    const token = await loginService.auth(login, password);
    res.json({ token });
  })
);

module.exports = router;
