const fs = require('fs');
const path = require('path');
const { promisify } = require('util');

const access = promisify(fs.access);
const mkdir = promisify(fs.mkdir);

const logOutput = {
  CONSOLE: 'CONSOLE',
  FILE: 'FILE'
};

// private
const LOGS_FOLDER_PATH = 'logs';
const LOGS_FILE_PATH = path.resolve(LOGS_FOLDER_PATH, 'app.log');
const INFO_LOGS_FILE_PATH = path.resolve(LOGS_FOLDER_PATH, 'info.log');
const WARN_LOGS_FILE_PATH = path.resolve(LOGS_FOLDER_PATH, 'warn.log');
const ERROR_LOGS_FILE_PATH = path.resolve(LOGS_FOLDER_PATH, 'error.log');

class Logger {
  constructor({ output = logOutput.CONSOLE }) {
    this.output = output;
    this.initialization = Promise.resolve(() => new Error('Logger is not initialized yet.'));
  }

  initLogFiles() {
    return (
      access(LOGS_FOLDER_PATH)
        .then(() => [
          fs.createWriteStream(LOGS_FILE_PATH, { flags: 'a' }),
          fs.createWriteStream(INFO_LOGS_FILE_PATH, { flags: 'a' }),
          fs.createWriteStream(WARN_LOGS_FILE_PATH, { flags: 'a' }),
          fs.createWriteStream(ERROR_LOGS_FILE_PATH, { flags: 'a' })
        ])
        .then(([fileStream, infoStream, warnStream, errorStream]) => {
          this.logStream = fileStream;
          this.infoStream = infoStream;
          this.warnStream = warnStream;
          this.errorStream = errorStream;
        })
        // if logs folder is not created yet, create it and retry init log files
        .catch(() => {
          mkdir(LOGS_FOLDER_PATH)
            .then(() => {
              this.initLogFiles();
            })
            // if no premissions to create folder, configure logger to write to the console
            .catch(() => {
              this.output = logOutput.CONSOLE;
              this.initLogConsole();
            });
        })
    );
  }

  initLogConsole() {
    return Promise.resolve().then(() => {
      this.logStream = process.stdout;
      this.infoStream = process.stdout;
      this.warnStream = process.stderr;
      this.errorStream = process.stderr;
    });
  }

  init() {
    switch (this.output) {
      case logOutput.FILE: {
        this.initialization = this.initLogFiles();
        break;
      }
      default: {
        this.initialization = this.initLogConsole();
      }
    }
  }

  log(type, message, additionalStream) {
    // check if logging output target was initialized
    return this.initialization.then(() => {
      this.logStream.write(`[${new Date().toUTCString()}] - [${type}] - ${message} \r\n`);
      additionalStream.write(`[${new Date().toUTCString()}] - [${type}] - ${message} \r\n`);
    });
  }

  info(message) {
    return this.log('INFO', message, this.infoStream);
  }

  warn(message) {
    return this.log('WARN', message, this.warnStream);
  }

  error(message) {
    return this.log('ERROR', message, this.errorStream);
  }
}

module.exports = { Logger, logOutput };
